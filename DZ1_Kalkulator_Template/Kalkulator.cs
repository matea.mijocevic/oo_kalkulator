﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PrvaDomacaZadaca_Kalkulator
{
    public class Factory
    {
        public static ICalculator CreateCalculator()
        {
            // vratiti kalkulator
            return new Kalkulator();
        }
    }

    public class Kalkulator:ICalculator
    {
        private string currentState; //mainOperand like string
        private double memory;
        private double mainOperand, secondOperand; //mainOperand je trenutni prikaz, a secondOperand broj s kojim se racunaju buduce operacije
        private char operation;
        private Boolean lastPressedNum, multipleEqual, processOfCreatingNum, commaBool; //flags

        CultureInfo c = new CultureInfo("hr-HR");

        /// <summary>
        /// Konstruktor
        /// </summary>
        public Kalkulator() 
        {
            ResetCalc();
        }

        /// <summary>
        /// Metoda za pritisak znaka/broja
        /// </summary>
        public void Press(char inPressedDigit)
        {
            if (Char.IsNumber(inPressedDigit))
            {
                PressNumber(inPressedDigit);
            }
            else
            {
                if (currentState != "-E-")
                {
                    switch (inPressedDigit)
                    {
                        case '+':
                        case '-':
                        case '*':
                        case '/':
                            Calculate(inPressedDigit);
                            operation = inPressedDigit;
                            if (lastPressedNum) lastPressedNum = false;
                            break;
                        case '=':
                            Calculate(inPressedDigit);
                            multipleEqual = true;
                            break;
                        case ',':
                            Comma();
                            break;
                        case 'M':
                            Sign();
                            break;
                        case 'S':
                        case 'K':
                        case 'T':
                        case 'Q':
                        case 'R':
                        case 'I':
                            ChangeMainOperand(inPressedDigit);
                            break;
                        case 'P':
                            PutInMemory();
                            break;
                        case 'G':
                            GetFromMemory();
                            break;
                        case 'C':
                            ClearCalc();
                            break;
                        case 'O':
                            ResetCalc();
                            break;
                        default:
                            ErrorFunction();
                            break;
                    }
                }
                else
                {
                    switch (inPressedDigit)
                    {
                        case 'G':
                            GetFromMemory();
                            break;
                        case 'C':
                            ClearCalc();
                            break;
                        case 'O':
                            ResetCalc();
                            break;
                        default:
                            ErrorFunction();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Vraca trenutno stanje u obliku stringa
        /// </summary>
        public string GetCurrentDisplayState()
        {
            return currentState;
        }

        /// <summary>
        /// Prilikom errora, memorija ostaje 
        /// </summary>
        public void ErrorFunction()
        {
            //memorija ostaje, ostatak se resetira
            double mem = memory;
            ResetCalc();
            currentState = "-E-";
            memory = mem;
            processOfCreatingNum = false;
        }

        /// <summary>
        /// Provjera ispravnosti rezulatata
        /// 3 ishoda:
        ///     - rezultat je ispravan -> return true
        ///     - nemoguc popravak -> return false, call ErrorFunction
        ///     - moguc popravak -> return false
        /// </summary>
        private bool IsValid(double checkValid)
        {
            //provjera cjelobrojnih brojeva
            if (checkValid < -9999999999 | checkValid > 9999999999)
            {
                ErrorFunction();
                return false;
            }

            //provjera broja znamenki: dobijemo apsolutni decimalni broj
            if (checkValid.ToString(c).Replace("-", string.Empty).Length > 11)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Provjerava ispravnost rezultata i ispravlja ga ako je to moguce
        /// </summary>
        private void SetCurrentDisplayState()
        {
            if (IsValid(mainOperand)) 
            {
                currentState = mainOperand.ToString(c);
            } 
            else if (currentState != "-E-") //ispravak rezultata
            {
                string pom = mainOperand.ToString(c);
                mainOperand = Math.Round(mainOperand, (10 - pom.Replace("-", String.Empty).IndexOf(",")));
                currentState = mainOperand.ToString(c);
            }
        }

        /// <summary>
        /// Operatori koji izazivaju mijenjanje operanada 
        /// Mijenja stanje objekta pomocu upravo pritisnutog operatora(digit) i proslog(operation)
        /// </summary>
        private void Calculate(char digit)
        {
            //drugi uvjet je potrebno dodati zbog slucaja ... = <operator> = ... jer se '=' ne upisuje u operator
            if ((lastPressedNum | digit == '=') & !(digit != '=' & multipleEqual))
            {
                double pom = mainOperand;
                switch (operation)
                {
                    case '+':
                        pom = mainOperand + secondOperand;
                        break;
                    case '-':
                        if ((digit == '=' & !lastPressedNum) | multipleEqual) pom = mainOperand - secondOperand;
                        else pom = secondOperand - mainOperand;
                        break;
                    case '*':
                        pom = mainOperand * secondOperand;
                        break;
                    case '/':
                        if ((digit == '=' & !lastPressedNum) | multipleEqual) { 
                            if(secondOperand == 0)
                            {
                                ErrorFunction();
                                break;
                            }
                            pom = mainOperand / secondOperand;
                        } else if (mainOperand == 0)
                        {
                            ErrorFunction();
                            break;
                        } else pom = secondOperand / mainOperand;
                        break;
                    case Char.MinValue:
                        break;
                }
                if (!multipleEqual & (digit != '=' | (lastPressedNum & digit == '='))) secondOperand = mainOperand;
                mainOperand = pom;
                //ako nam operacija nije izazvala error
                if (GetCurrentDisplayState() != "-E-") SetCurrentDisplayState();
            }
            else secondOperand = mainOperand;

            processOfCreatingNum = false;
            commaBool = false;
        }

        /// <summary>
        /// Operatori koji samo mijenjaju stanje mainOperand-a i currentState-a 
        /// Metoda se gleda kao da je upravo pritisnut broj
        /// </summary>
        private void ChangeMainOperand(char funct)
        {
            switch(funct){
                case 'S':
                    if (Double.IsNaN(Math.Sin(mainOperand)))
                    {
                        ErrorFunction();
                        break;
                    }
                    mainOperand = Math.Sin(mainOperand);
                    break;
                case 'K':
                    if (Double.IsNaN(Math.Cos(mainOperand)))
                    {
                        ErrorFunction();
                        break;
                    }
                    mainOperand = Math.Cos(mainOperand);
                    break;
                case 'T':
                    if (Double.IsNaN(Math.Tan(mainOperand)))
                    {
                        ErrorFunction();
                        break;
                    }
                    mainOperand = Math.Tan(mainOperand);
                    break;
                case 'Q':
                    mainOperand *= mainOperand;
                    break;
                case 'R':
                    if (Double.IsNaN(Math.Sqrt(mainOperand)))
                    {
                        ErrorFunction();
                        break;
                    }
                    mainOperand = Math.Sqrt(mainOperand);
                    break;
                case 'I':
                    if(mainOperand == 0)
                    {
                        ErrorFunction();
                        break;
                    }
                    mainOperand = 1/mainOperand;
                    break;
            }
            processOfCreatingNum = false;
            lastPressedNum = true;
            commaBool = false;
            if(GetCurrentDisplayState() != "-E-") SetCurrentDisplayState();
        }

        /// <summary>
        /// Pritisak na zarez
        /// </summary>
        private void Comma()
        {
            commaBool = true;
            currentState += ",";
        }

        /// <summary>
        /// Pritisak na M
        /// </summary>
        private void Sign()
        {
            mainOperand *= -1;
            SetCurrentDisplayState();
        }

        /// <summary>
        /// Pritisak na broj
        /// </summary>
        public void PressNumber(char charNum)
        {
            int intNum = int.Parse(charNum.ToString());
            if(processOfCreatingNum) 
            {
                //s brojem znamenaka < 10
                if ((currentState.Replace("-", string.Empty).Replace(",", string.Empty)).Length < 10)
                {
                    if (commaBool)
                    {
                        if(currentState.IndexOf(",") == -1)
                            currentState += ",";
                    }
                    currentState += charNum.ToString();
                    mainOperand = double.Parse(currentState.Replace(',','.'));
                    //ispravlja rezultat
                    currentState = (currentState.Length > 1 & currentState.StartsWith("0") & currentState[1] != ',') ?
                        currentState.Substring(1) : currentState;
                }
            }
            else
            {
                if (!lastPressedNum)
                {
                    secondOperand = mainOperand;
                }
                mainOperand = intNum;
                SetCurrentDisplayState();
                multipleEqual = false;
                lastPressedNum = processOfCreatingNum = true;
            }
        }

        /// <summary>
        /// Brisanje prikaza, ali ostaje sve ostalo
        /// </summary>
        private void ClearCalc()
        {
            mainOperand = 0;
            commaBool = false;
            SetCurrentDisplayState();
            processOfCreatingNum  = false;
        }

        /// <summary>
        /// Vracanje na pocetno stanje
        /// </summary>
        private void ResetCalc()
        {
            currentState = "0";
            memory = double.MinValue;
            operation = Char.MinValue; 
            mainOperand = secondOperand = 0;
            lastPressedNum = processOfCreatingNum = true;
            multipleEqual = false;
            commaBool = false;
        }

        /// <summary>
        /// Stavljanje u memoriju
        /// </summary>
        private void PutInMemory()
        {
            if (currentState != "-E-")
            {
                memory = mainOperand;
                processOfCreatingNum = true;
            }
        }

        /// <summary>
        /// Dohvacanje iz memorije
        /// </summary>
        private void GetFromMemory()
        {
            if (memory != double.MinValue)
            {
                mainOperand = memory;
                SetCurrentDisplayState();
                processOfCreatingNum = false;
            }
            else
            {
                ErrorFunction();
            }
        }
    }
}
